#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
//import { CdkWorkshopTutorialStack } from '../lib/cdk-workshop-tutorial-stack';
import { WorkshopPipelineStack } from '../lib/pipeline-stack';

const app = new cdk.App();
new WorkshopPipelineStack(app, 'CdkWorkshopPipelineStack', {
  description: "Softcomweb cdk stack for Lambda and Api Gateway",
  stackName: "Softcomweb-Workshop-cdk-pipline-",
  tags: {
    "team": "softcomweb",
    "env": "infra"
  },
  env: {
    region: "eu-central-1"
  }
});
