import { expect as expectCDK, haveResource } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import * as CdkWorkshopTutorial from '../lib/cdk-workshop-tutorial-stack';

test('has lambda function', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new CdkWorkshopTutorial.CdkWorkshopTutorialStack(app, 'MyTestStack');
    // THEN
    expectCDK(stack).to(haveResource("AWS::Lambda::Function",{
    }));
});
// test('SNS Topic Created', () => {
//   const app = new cdk.App();
//   // WHEN
//   const stack = new CdkWorkshopTutorial.CdkWorkshopTutorialStack(app, 'MyTestStack');
//   // THEN
//   expectCDK(stack).to(haveResource("AWS::SNS::Topic"));
// });
