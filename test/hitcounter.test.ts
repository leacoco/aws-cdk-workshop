import { expect as expectCDK, haveResource } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as dynamodb from '@aws-cdk/aws-dynamodb';

import { HitCounter } from '../lib/hitcounter';

test('Dynamodb Table Created', () => {
  const stack = new cdk.Stack();

  new HitCounter(stack, 'TestConstruct', {
    downStreamFunction: new lambda.Function(stack, 'TestFunction', {
      runtime: lambda.Runtime.NODEJS_10_X,
      handler: 'lambda.handler',
      code: lambda.Code.fromInline('test')
    })
  });

  expectCDK(stack).to(haveResource('AWS::DynamoDB::Table'));
});

test('lambda has environment variables', () => {
  const stack = new cdk.Stack();
  const downstreamFunction =  new lambda.Function(stack, 'TestFunction', {
    runtime: lambda.Runtime.NODEJS_10_X,
    handler: 'lambda.handler',
    code: lambda.Code.fromInline('test')
  })
  //When
  new HitCounter(stack, 'TestConstruct', {
    downStreamFunction: downstreamFunction,
  });

  //Then
  expectCDK(stack).to(haveResource("AWS::Lambda::Function", {
    Environment: {
      Variables: {
        DOWNSTREAM_FUNCTION_NAME: {"Ref": "TestFunction22AD90FC"},
        HITS_TABLE_NAME: {"Ref": "TestConstructHitsTable47BB8B6A"}
      }
    }
  }))
});

test('Read capacity can be configured', () => {
  const stack = new cdk.Stack();
  expect(() => {
    new HitCounter(stack, 'TestConstruct', {
      downStreamFunction: new lambda.Function(stack, 'TestFunction', {
        runtime: lambda.Runtime.NODEJS_10_X,
        code: lambda.AssetCode.fromInline('test'),
        handler: 'lambda.handler'
      }),
      readCapacity: 3
    });
  }).toThrowError('Readcapacity should be in range between 5 and 20')
})

