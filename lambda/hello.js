exports.handler = async function(event) {
  console.log("Requests: ", JSON.stringify(event, undefined, 2));
  return {
    statusCode: 200,
    headers: {
      "Content-Type": "text/plain",
    },
    body: JSON.stringify({
      "message": "Hello from Softcomweb tutorial",
      "user": "leadel kolo"
    })
  };
};
