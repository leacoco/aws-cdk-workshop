# Welcome to your CDK TypeScript project!

You should explore the contents of this project. It demonstrates a CDK app with an instance of a stack (`CdkWorkshopTutorialStack`)
which contains an Amazon SQS queue that is subscribed to an Amazon SNS topic.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template


NOTE:

The construct @aws-cdk/pipelines uses new core CDK framework features called “new style stack synthesis”. In order to deploy our pipeline, we must enable this feature in our CDK configuration.

edit the file cdk.json
{
    "app": "node bin/cdk-workshop.js",
    "context": {
        "@aws-cdk/core:newStyleStackSynthesis": true
    }
}

