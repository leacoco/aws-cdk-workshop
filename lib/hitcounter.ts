import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as dynamodb from '@aws-cdk/aws-dynamodb';

export interface HitCounterProps {
  downStreamFunction: lambda.IFunction;
  readCapacity?: number //Must be greater than 5 and less than 20. Default is 5
}

export class HitCounter extends cdk.Construct {

  public readonly handler: lambda.IFunction
  public readonly table: dynamodb.Table

  constructor(scope: cdk.Construct, id: string, props: HitCounterProps) {
    if (props.readCapacity !== undefined && (props.readCapacity < 5 || props.readCapacity > 20)) {
      throw new Error('Readcapacity should be in range between 5 and 20')
    }
    super(scope, id);

    this.table = new dynamodb.Table(this, 'HitsTable', {
      partitionKey: {
        name: 'path',
        type: dynamodb.AttributeType.STRING
      },
      readCapacity: props.readCapacity || 5
    });

    this.handler = new lambda.Function(this, 'HitCounterHandler', {
      runtime: lambda.Runtime.NODEJS_10_X,
      handler: 'hitcounter.handler',
      code: lambda.Code.fromAsset('lambda'),
      environment: {
        DOWNSTREAM_FUNCTION_NAME: props.downStreamFunction.functionName,
        HITS_TABLE_NAME: this.table.tableName
      }
    });

    //Grant the lambda role read/write permissions to our table
    this.table.grantReadWriteData(this.handler);

    //Grant lambda permissions to invoke downstream function
    props.downStreamFunction.grantInvoke(this.handler);
  }
}
