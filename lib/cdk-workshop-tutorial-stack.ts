import * as lambda from '@aws-cdk/aws-lambda';
import * as apigw from '@aws-cdk/aws-apigateway';
import * as cdk from '@aws-cdk/core';
import { HitCounter } from './hitcounter';
import { TableViewer } from 'cdk-dynamo-table-viewer';

export class CdkWorkshopTutorialStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const helloLambda = new lambda.Function(this, "HelloLambdaFunction", {
      runtime: lambda.Runtime.NODEJS_10_X,
      code: lambda.Code.fromAsset('lambda'),
      handler: "hello.handler",
    });


    const hitCounterLambda = new HitCounter(this, 'HitCounterFuntion', {
      downStreamFunction: helloLambda
    });

    new apigw.LambdaRestApi(this, "Endpointid", {
      handler: hitCounterLambda.handler
    });

    new TableViewer(this, 'ViewerTable', {
      title: 'Hello Hits',
      table: hitCounterLambda.table
    })
  }
}
