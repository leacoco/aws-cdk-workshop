import * as cdk from '@aws-cdk/core';
import * as codecommit from '@aws-cdk/aws-codecommit';
import * as codepipeline from '@aws-cdk/aws-codepipeline';
import * as codepipeline_actions from '@aws-cdk/aws-codepipeline-actions';
import { SimpleSynthAction, CdkPipeline } from '@aws-cdk/pipelines';
import { WorshopPipelineStage } from './pipeline-stage';

export class WorkshopPipelineStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const repo =  new codecommit.Repository(this, 'WorkshopRepo', {
      repositoryName: 'SoftcomwebWorkshop',
      description: 'Softcomweb workshop repository',
    });

    const sourceArtifact = new codepipeline.Artifact();
    const cloudAssemblyArtifact = new codepipeline.Artifact();

    const pipeline = new CdkPipeline(this, 'pipeline', {
      cloudAssemblyArtifact,
      pipelineName: 'SoftcomwebWorkshopPipeline',
      sourceAction: new codepipeline_actions.CodeCommitSourceAction({
        actionName: 'CodeCommit',
        output: sourceArtifact,
        repository: repo
      }),
      synthAction: SimpleSynthAction.standardNpmSynth({
        sourceArtifact,
        cloudAssemblyArtifact,
        buildCommand: 'npm run build'
      })
    });

    const deploy = new WorshopPipelineStage(this, 'Deploy');
    pipeline.addApplicationStage(deploy);

  }
}
