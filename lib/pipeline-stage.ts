import { CdkWorkshopTutorialStack } from './cdk-workshop-tutorial-stack';
import { Stage, Construct, StackProps } from '@aws-cdk/core';

export class WorshopPipelineStage extends Stage {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    new CdkWorkshopTutorialStack(this, 'Webserver')
  }
}
